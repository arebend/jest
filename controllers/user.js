const { User } = require('../models');

module.exports = {
    register(req, res) {
        User.create({
            email: req.body.email,
            encrypted_password: req.body.password
        })
            .then(user => {
                res.status(201).json({
                    status: 'success',
                    data: { user }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: "fail",
                    errors: [err.message]
                })
            })
    },
}