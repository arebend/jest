const app = require('../server'); // Import server
const supertest = require('supertest');
const request = supertest(app); // Create request instance
const { User } = require('../models'); // Require the Post model

describe('Post API Collection', () => {

    beforeAll(() => {
        return User.destroy({ truncate: true })
    })
    afterAll(() => {
        return User.destroy({ truncate: true })
    })

    describe('POST /register', () => {
        test('Should succesfully register new user', done => {
            request.post('/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'galih@mail.com',
                    password: '123456'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should unsuccesfully register new user', done => {
            request.post('/register')
                .set('Content-type', 'application/json')
                .send({
                    email: 'galih@mail.com',
                    encrypted_password: '123456'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})