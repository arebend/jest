const app = require('../server'); // Import server
const supertest = require('supertest');
const request = supertest(app); // Create request instance
const { Post } = require('../models'); // Require the Post model

describe('Post API Collection', () => {

    beforeAll(() => {
        return Post.destroy({ truncate: true })
    })

    afterAll(() => {
        return Post.destroy({ truncate: true })
    })

    describe('POST /postcreate', () => {
        test('Should succesfully create new post', done => {
            request.post('/postcreate')
                .set('Content-Type', 'application/json')
                .send({
                    title: 'Hello World',
                    body: 'Lorem Ipsum'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should unsuccesfully create new post', done => {
            request.post('/postcreate')
                .set('Content-type', 'application/json')
                .send({
                    title: 'Hello Wold',
                    body: null
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('PUT /post/:id', () => {
        test('Should succesfully update post', done => {
            request.put('/post/1')
                .set('Content-type', 'application/json')
                .send({
                    title: 'Hello World',
                    body: 'Lorem Ipsum'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(202);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should unsuccesfully update post', done => {
            request.put('/post/1')
                .set('Content-type', 'application/json')
                .send({
                    title: null,
                    body: ''
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    }),
        describe('DELETE /post/:idPost', () => {
            test('Should succesfully delete post', done => {
                request.delete('/delete/1')
                    .set('Content-type', 'application/json')
                    .then(res => {
                        expect(res.statusCode).toEqual(204);
                        // expect(res.body.status).toEqual('success');
                        done();
                    })
            })
            test('Should unsuccesfully delete post', done => {
                request.delete(`/delete/"1"`)
                    .set('Content-type', 'application/json')
                    .then(res => {
                        expect(res.statusCode).toEqual(422);
                        expect(res.body.status).toEqual('fail');
                        done();
                    })
            })
        })


})
