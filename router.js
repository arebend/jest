const router = require('express').Router();

// Import the controller
const cont_index = require('./controllers/index');
const cont_post = require('./controllers/post');
const cont_user = require('./controllers/user')

// Root Endpoint
router.get('/', cont_index.home);

router.get('/findpost', cont_post.index);
router.post('/postcreate', cont_post.create);
router.put('/post/:id', cont_post.update);
router.delete('/delete/:id', cont_post.delete);

router.post('/register', cont_user.register)


module.exports = router;
